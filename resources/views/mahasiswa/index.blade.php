@extends('layout/main')

@section('judul','Daftar Mahasiswa')

@section ('container')    
</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-10">

    <h1 class="mt-3">DAFTAR MAHASISWA</h1>


   <table class="table">
    <thead class="thead-dark">
    <tr>
		<th scope="col">No.</th>
		<th scope="col ">Nama</th>
		<th scope="col">NRP</th>
		<th scope="col">Email</th>
		<th scope="col">Prodi</th>
	</tr>

    </thead>

<tbody>
	@foreach($mahasiswa as $mhs) 
	<tr>
		<th scope="row">{{ $loop->iteration }}</th>
		<td> {{ $mhs->nama }}</td>
		<td> {{ $mhs->nrp }}</td>
		<td> {{ $mhs->email }}</td>
		<td> {{ $mhs->prodi }} </td>
	</tr>
@endforeach
</tbody>
 

    </table>

</div>
</div>
</div>
   @endsection