@extends('layout/main')

@section('judul','Form Tambah Data Mahasiswa')

@section ('container')    
<div class="container">
	<div class="row">
		<div class="col-10">

    <h1 class="mt-3">Form Tambahan Data Mahasiswa</h1>
    <form method="post" action="/students">
    @csrf
    <div class="form-group">
    <label for="nama">Nama</label>
    <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" placeholder="Masukkan Nama" name="nama" value="{{old ('nama') }}">
    @error('nama')<div class="invalid-feedback">{{ $message }}
    </div>@enderror
    </div>
    <div class="form-group">
    <label for="nrp">NRP</label>
    <input type="text" class="form-control @error('nrp') is-invalid @enderror" id="nrp" placeholder="Masukkan nrp" name="nrp" value="{{old ('nrp') }}">
    @error('nrp')<div class="invalid-feedback">{{ $message }}
    </div>@enderror
    </div>
    <div class="form-group">
    <label for="email">Email</label>
    <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Masukkan Email" name="email" value="{{old ('email') }}">
    @error('email')<div class="invalid-feedback">{{ $message }}
    </div>@enderror
    </div>
    <div class="form-group">
    <label for="prodi">Prodi</label>
    <input type="text" class="form-control @error('prodi') is-invalid @enderror" id="prodi" placeholder="Masukkan Prodi" name="prodi" value="{{old ('prodi') }}">
    @error('prodi')<div class="invalid-feedback">{{ $message }}
    </div>@enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah Data!</button>
    </form>
    </div>
  </div>
</div>
@endsection